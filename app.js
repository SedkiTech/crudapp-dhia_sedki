const express = require("express");
const app = express();
const morgan = require("morgan");
//import routes
const projectRouter = require("./routes/projectRoutes");

// middleware
app.use(express.json());
app.use(morgan("dev"));

// mount routes
app.use("/api/v1/projects", projectRouter);

// start the server
const port = 3001;
app.listen(port, () => {
  console.log(`app running on port ${port}`);
});
